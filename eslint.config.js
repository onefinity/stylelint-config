import configure from '@onefinity/eslint-config';

export default configure([{
    rules: {
        '@onefinity/eslint-config/consistent-array-of-objects-notation': 'off',
        'unicorn/no-null': 'off'
    }
}]);
