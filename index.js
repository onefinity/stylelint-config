export default {
    extends: [
        '@stylistic/stylelint-config',
        'stylelint-config-standard-scss'
    ],
    plugins: [
        'stylelint-order'
    ],
    reportDescriptionlessDisables: true,
    reportInvalidScopeDisables: true,
    reportNeedlessDisables: true,
    reportUnscopedDisables: true,
    overrides: [{
        files: ['*.scss', '**/*.scss'],
        customSyntax: 'postcss-scss',
        rules: {
            '@stylistic/block-closing-brace-newline-after': ['always', {
                ignoreAtRules: ['if', 'else']
            }],
            '@stylistic/function-comma-newline-after': null,
            '@stylistic/function-parentheses-newline-inside': null,
            '@stylistic/indentation': 4,
            '@stylistic/max-line-length': null,
            '@stylistic/string-quotes': 'single',
            'alpha-value-notation': 'number',
            'at-rule-empty-line-before': ['always', {
                except: ['first-nested', 'blockless-after-blockless'],
                ignore: ['after-comment'],
                ignoreAtRules: ['if', 'else']
            }],
            'font-family-name-quotes': 'always-unless-keyword',
            'no-descending-specificity': null,
            'no-duplicate-selectors': null,
            'order/properties-order': [
                [
                    'content',
                    'position',
                    'z-index',
                    'inset',
                    {
                        order: 'flexible',
                        properties: [
                            'top',
                            'right',
                            'bottom',
                            'left'
                        ]
                    },
                    'display',
                    'float',
                    'clear',
                    'box-sizing',
                    {
                        order: 'flexible',
                        properties: [
                            'grid',
                            'grid-template',
                            'grid-template-rows',
                            'grid-template-columns',
                            'grid-template-areas',
                            'grid-auto-columns',
                            'grid-auto-rows',
                            'grid-auto-flow',
                            'grid-row',
                            'grid-column',
                            'grid-area',
                            'grid-gap',
                            'grid-row-gap',
                            'grid-column-gap',
                            'grid-row-start',
                            'grid-row-end',
                            'grid-column-start',
                            'grid-column-end'
                        ]
                    }, {
                        order: 'flexible',
                        properties: [
                            'flex',
                            'flex-grow',
                            'flex-shrink',
                            'flex-basis',
                            'flex-flow',
                            'flex-direction',
                            'flex-wrap',
                            'order',
                            'justify-items',
                            'justify-content',
                            'justify-self',
                            'align-items',
                            'align-content',
                            'align-self'
                        ]
                    }, {
                        order: 'flexible',
                        properties: [
                            'table-layout',
                            'vertical-align'
                        ]
                    },
                    'width',
                    'min-width',
                    'max-width',
                    'height',
                    'min-height',
                    'max-height',
                    {
                        order: 'flexible',
                        properties: [
                            'padding',
                            'padding-top',
                            'padding-right',
                            'padding-bottom',
                            'padding-left',
                            'margin',
                            'margin-top',
                            'margin-right',
                            'margin-bottom',
                            'margin-left'
                        ]
                    }, {
                        order: 'flexible',
                        properties: [
                            'border',
                            'border-width',
                            'border-style',
                            'border-color',
                            'border-top',
                            'border-top-width',
                            'border-top-style',
                            'border-top-color',
                            'border-right',
                            'border-right-width',
                            'border-right-style',
                            'border-right-color',
                            'border-bottom',
                            'border-bottom-width',
                            'border-bottom-style',
                            'border-bottom-color',
                            'border-left',
                            'border-left-width',
                            'border-left-style',
                            'border-left-color',
                            'border-radius',
                            'border-top-left-radius',
                            'border-top-right-radius',
                            'border-bottom-right-radius',
                            'border-bottom-left-radius'
                        ]
                    }, {
                        order: 'flexible',
                        properties: [
                            'outline',
                            'outline-width',
                            'outline-style',
                            'outline-color'
                        ]
                    }, {
                        order: 'flexible',
                        properties: [
                            'overflow',
                            'overflow-x',
                            'overflow-y'
                        ]
                    },
                    'transform'
                ], {
                    unspecified: 'bottom'
                }
            ],
            'scss/at-mixin-argumentless-call-parentheses': 'always',
            'selector-class-pattern': ['^[a-z][a-zA-Z0-9]*$', {
                resolveNestedSelectors: true
            }],
            'selector-pseudo-class-no-unknown': [true, {
                ignorePseudoClasses: ['global', 'local', 'export']
            }],
            'selector-pseudo-element-colon-notation': 'single'
        }
    }]
};
