# Onefinity Stylelint Config
Generic base configuration for [Stylelint][stylelint], supports [Sass][sass]
and [CSS Modules][css-modules]. Combine it with
[`@onefinity/eslint-config`][onefinity-eslint-config] to lint JavaScript as
well.


## Available configurations
The following configurations are available and can be extended in a
project-specific Stylelint configuration.

- [`@onefinity/stylelint-config`](./index.js)  
  Base configuration for projects using CSS or [Sass][sass].


## Usage
### Installation
Install the package using the following command.

```shell
npm install --save-dev @onefinity/stylelint-config
```

### Extending
Use the [`extends`][stylelint-extends] feature provided by Stylelint in the
project-specific `stylelint.config.mjs` configuration file.

```js
export default {
    extends: '@onefinity/stylelint-config'
};
```

### Scripts
The following scripts can be added to `package.json` to make it easier to start
the linter.

```json
{
    "scripts": {
        "lint:css": "stylelint \"source/**/*.scss\"",
        "lint:fix": "npm run lint:css -- --fix"
    }
}
```



[stylelint]: https://stylelint.io/
[stylelint-extends]: https://stylelint.io/user-guide/configure/#extends
[sass]: https://sass-lang.com/
[css-modules]: https://github.com/css-modules/css-modules
[onefinity-eslint-config]: http://npmjs.com/package/@onefinity/eslint-config
