import stylelint from 'stylelint';
import { it } from 'node:test';
import { expect } from 'expect';

it('lints without errors', async () => {
    const configuration = await import('./index.js');
    const result = await stylelint.lint({
        config: configuration.default,
        code: `.test { color: red; }`
    });

    expect(result.results.at(0).warnings).toEqual(expect.arrayContaining([
        expect.objectContaining({
            rule: '@stylistic/no-missing-end-of-source-newline',
            severity: 'error'
        })
    ]));
});
